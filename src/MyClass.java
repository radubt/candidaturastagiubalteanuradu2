import java.util.*;
import java.util.Scanner;

public class MyClass {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter how many numbers you want to check: ");
		
		int num = scan.nextInt();
		int[] numbers = new int[(int) num];
		
		
		for(int i=0;i<numbers.length;i++){
			System.out.println("Please enter number "+(i+1));
			numbers[i]=scan.nextInt();
			
		}
		int[] numbers2 = new int[(int) num];
		System.arraycopy(numbers, 0, numbers2, 0, numbers.length);	
		
		BubbleSort sort = new BubbleSort();
		sort.bubbleSort(numbers);
		System.out.println(Arrays.toString(numbers));
		System.out.println("Smallest number is: " + numbers[0]);
		System.out.println("Largest number is: " + numbers[numbers.length-1]);
		
		
		List<Integer> list = new ArrayList<Integer>();
		
		
		
		for(int i : numbers){
			boolean isPrime = true;
			
			for(int j=2;j<i;j++){
				if(i%j==0)
					isPrime = false;				
			}
			if (isPrime){
				list.add(i);
			}
		}
		System.out.println("Prime numbers are: "+list);
		
		List<Integer> list2 = new ArrayList<Integer>();

		for(int x : numbers2){
			int palindrome = x;
			int reverse = 0, rmd;
			
			while(palindrome != 0){
				rmd = palindrome%10;
				reverse = reverse*10 + rmd;
				palindrome = palindrome/10;
			}
			if(x == reverse)
				list2.add(x);
		}
		System.out.println("Palindromes are: "+list2);
	}
}
